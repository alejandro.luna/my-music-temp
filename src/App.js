import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import Routing from "./utils/routes/routing";

function App() {
  return (
    <Router>
      <Routing />
    </Router>
  );
}

export default App;
