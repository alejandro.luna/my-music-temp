import React from "react"
import "./items-grid.scss"
import AlbumCard from "../../atoms/album-card/album-card"
import SongCard from "../../atoms/song-card/song-card"

const ItemsGrid = props => {
  const { songs, isAlbum, favs, title, setActualPlayer } = props
  const gridClass = `${isAlbum ? "album" : "songs"}`
  return (
    <div className={gridClass}>
      {!isAlbum &&
        songs &&
        songs.map((song, index) => {
          return (
            <SongCard
              key={index}
              song={song?.track}
              index={index}
              title={title}
              favs={favs}
              setActualPlayer={setActualPlayer}
            />
          )
        })}
      {isAlbum &&
        songs &&
        songs.map((song, index) => {
          return (
            <AlbumCard
              key={index}
              song={song}
              setActualPlayer={setActualPlayer}
            />
          )
        })}
    </div>
  )
}

export default ItemsGrid
