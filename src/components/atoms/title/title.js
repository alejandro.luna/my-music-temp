import React from "react"
import logoImg from "../../../assets/Images/musicLogoC.png"
import "./title.scss"

const Title = () => {
  return (
    <div className="title">
      <img src={logoImg} className="title__imag" alt="Music logo" />
      <h1 className="title__name">My Music</h1>
    </div>
  )
}

export default Title
