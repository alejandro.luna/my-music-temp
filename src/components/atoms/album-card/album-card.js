import React from "react"
import "./album-card.scss"

const AlbumCard = props => {
  const { song, setActualPlayer } = props
  const playAlbum = id => {
    setActualPlayer(`playlist/${id}`)
    localStorage.setItem("actual", `playlist/${id}`)
  }
  return (
    <div className="card" onClick={() => playAlbum(song.id)}>
      <img src={song.images[0].url} className="card__imag" alt="Music logo" />
      <h2 className="card__name">{song.name}</h2>
    </div>
  )
}

export default AlbumCard
