import React, { useEffect, useState } from "react"
import { services } from "../../../services/services"
import "./header.scss"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBars } from "@fortawesome/free-solid-svg-icons"

import { ReactComponent as LogoutIcon } from "../../../assets/Images/user-large-slash-solid.svg"
import myImg from "../../../assets/Images/musicLogoC.png"

const Header = props => {
  const { setIsMenuClosed, title, subtitle } = props

  const [userData, setUserData] = useState(null)
  const [img, setImg] = useState(
    "https://cdn-icons-png.flaticon.com/512/1160/1160922.png"
  )

  const userName = `${userData ? userData?.display_name : "Cargando..."}`

  useEffect(() => {
    services.getMenu(setUserData, setImg)
  }, [])

  return (
    <header>
      <div className="header">
        <div className="header__invisible"></div>
        <div className="header__logo">
          <img src={myImg} className="header__imag" alt="Music logo" />
          <h1 className="header__name">My Music</h1>
        </div>

        <div className="header__menu">
          <a href="/home" className="header__option">
            Home
          </a>
          <a href="/favs" className="header__option">
            Favs
          </a>
          <a href="/albums" className="header__option">
            Albums
          </a>
        </div>

        <div className="header__logout">
          <b className="header__user">
            <img src={img} alt="UserImage" className="header__user-image" />
            {userName}
          </b>
          <a href="/" className="header__logout-button">
            <LogoutIcon className="header__logout-icon" />
          </a>
        </div>

        <FontAwesomeIcon
          className="header__icon"
          icon={faBars}
          onClick={() => setIsMenuClosed(false)}
        />
      </div>
      <h2 className="section">{title}</h2>
      <h3 className="subSection">{subtitle}</h3>
    </header>
  )
}

export default Header
