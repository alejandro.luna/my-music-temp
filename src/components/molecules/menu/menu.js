import React, { useEffect, useState } from "react"
import { services } from "../../../services/services"
import "./menu.scss"

import { ReactComponent as XIcon } from "../../../assets/Images/xmark-solid.svg"
import { ReactComponent as LogoutIcon } from "../../../assets/Images/user-large-slash-solid.svg"

const Menu = props => {
  const { isMenuClosed, setIsMenuClosed } = props
  const [userData, setUserData] = useState(null)
  const [img, setImg] = useState(
    "https://cdn-icons-png.flaticon.com/512/1160/1160922.png"
  )
  const menuClass = `menu ${
    isMenuClosed ? "close-animate-menu" : "open-animate-menu"
  }`
  const userName = `${userData ? userData?.display_name : "Cargando..."}`

  useEffect(() => {
    services.getMenu(setUserData, setImg)
  }, [])

  return (
    <div className={menuClass}>
      <XIcon className="menu__icon" onClick={() => setIsMenuClosed(true)} />

      <div className="menu__container">
        <a href="/home" className="menu__options">
          Home
        </a>
        <a href="/favs" className="menu__options">
          Favs
        </a>
        <a href="/albums" className="menu__options">
          Albums
        </a>
      </div>

      <div className="menu__logout">
        <b className="menu__user">
          <img src={img} alt="UserImage" className="menu__user-image" />
          {userName}
        </b>
        <a href="/" className="menu__logout-button">
          <LogoutIcon className="menu__logout-icon" />
          Cerrar sesión
        </a>
      </div>
    </div>
  )
}

export default Menu
